 var billingId = context.getVariable("request.queryparam.billingId");
print(billingId);

var last = context.getVariable("request.queryparam.last");
var regexLast = new RegExp("^(true|false|TRUE|FALSE|True|False)$");

var startDate = context.getVariable("request.queryparam.startDate");

var endDate = context.getVariable("request.queryparam.endDate");

context.setVariable("isValidBillingId","false");

var regex = new RegExp(/^[0-9]{4,}$/);

var path = "/billingaccounts/"+ billingId +"/history";

print(path);
context.setVariable("apiPath",path);

if (billingId === null || billingId === "") {
    context.setVariable("isRequestValid","false");
    context.setVariable("errorMessage", "Please provide a valid billingId (numeric value and minimum length should be 4)");
    context.setVariable("queryParam", "billingId");
    
} else {
var billingIds = billingId.split(",");
(function validateBillingId(){
for(var i=0;i<billingIds.length;i++){
if(!regex.test(billingIds[i])){
context.setVariable("isRequestValid","false");
    context.setVariable("queryParam", "billingId");
    context.setVariable("errorMessage", "One of the billingId is invalid, Invalid billingId " 
    + billingIds[i]);
if(billingIds[i]==="") {
    context.setVariable("errorMessage", "One of the billingId is invalid, BillingId cannot be empty.") 
}
return "false";
}}

}())
}

if(last!==null && !regexLast.test(last)) {
    context.setVariable("isRequestValid", "false");
    context.setVariable("queryParam", "last");
    context.setVariable("errorMessage", "Invalid last value is provided.");
}

if (startDate !== null && !moment(startDate).isValid()) { 
    context.setVariable("isRequestValid", "false");
    context.setVariable("errorMessage", "Please provide valid startDate."); 
    context.setVariable("queryParam", "startDate");
    
}  

if (endDate !== null && !moment(endDate).isValid()) { 
    context.setVariable("isRequestValid", "false");
    context.setVariable("errorMessage", "Please provide valid endDate."); 
    context.setVariable("queryParam", "endDate");
    
}

if (endDate !== null && startDate!==null && last!==null 
&&moment(startDate).isBefore(endDate) && last.toLowerCase()==="true") { 
    context.setVariable("invalidParam", "true");
    context.setVariable("errorMessage", "Exception: If Start date and End date are" 
    + " provided, then Last must not be TRUE. Start Date:"+ startDate+", "
    +"End Date:"+ endDate+", Last: true"); 
    context.setVariable("subcode", "-4");
}