 var headers = {};
headers.authorization = context.getVariable("request.header.authorization");

var http = {};
http.headers = headers;
http.method = context.getVariable("request.verb");
http.path = context.getVariable("apiPath");
http.audienceId = context.getVariable("private.audienceId");
http.pem = context.getVariable("private.pem");
http.keyMappingsUrl =  "http://"+context.getVariable("private.keymapping-eks-ip")+"/api/v2/customers/"+context.getVariable("request.queryparam.billingId")+"/keylookup?idtype=BILLING_ID";

var request = {};
request.http = http;

var attributes = {};
attributes.request = request;

var opa_request = {};
opa_request.attributes = attributes;

context.setVariable("opa_request", JSON.stringify(opa_request));